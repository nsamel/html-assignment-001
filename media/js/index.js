$(function () {
	var categories;
	var tasks;
	var selectedCategoryId;

	//Populates Category list
    $.get("media/data/categories.json", function(data) {
        categories = data.categories;
		genrate_categories(categories);
    });

	$.get("media/data/tasks.json", function(data) {
		tasks = data;
		genrate_tasks(101);
	});

	function genrate_categories(local_categories) {
		var list_Options = '';
        $.each(local_categories, function(index, element) {
			list_Options += '<li><a id='+ element.categoryId +' name="' + element.categoryName + '" href="#">' + element.categoryId + ' - ' + element.categoryName + ' (' + element.numberOfTasks + ')</a></li>';
        });
		$(".catagories").html(list_Options);
	}

	function genrate_tasks(categoryId) {
		selectedCategoryId = categoryId;
		var list_Options = '';
		$.each(tasks, function(index, element) {
			if(element[categoryId]) {
				$.each(element[categoryId], function(index, task) {	
					list_Options += '<tr>';
					list_Options += '<td><input type="checkbox" name="task[]"></td>';
					list_Options += '<td>'+ task.taskName +'</td>'
					list_Options += '<td>'+ task.deadline +'</td>';
					list_Options += '</tr>';
				});
			}
        });
		$(".tasks").html(list_Options);        
	}

	// Attach a delegated event handler
	$( ".catagories" ).on( "click", "a", function( event ) {
		event.preventDefault();
		$("#category-title").html( $( this ).attr("name") );
		genrate_tasks($( this ).attr("id"));
	});

	$( "#taskForm" ).submit(function( event ) {
		var newTask = {
			taskId :  Math.random(5),
			taskName : $("#taskName").val(),
			deadline: $("#deadlineDate").val()
		}
		tasks.tasks[selectedCategoryId].push(newTask);		
		genrate_tasks(selectedCategoryId);
        $('[data-popup="popup-1"]').fadeOut(350);
		event.preventDefault();
	});

    $('[data-popup-open]').on('click', function (e) {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
        e.preventDefault();
    });

    $('[data-popup-close]').on('click', function (e) {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
        e.preventDefault();
    });

    $("#searchText").keyup(function(){
		filter = new RegExp($(this).val(),'i');
		$("#filterme tbody tr").filter(function(){
			$(this).each(function(){
				found = false;
				$(this).children().each(function(){
					content = $(this).html();
					if(content.match(filter)) {
						found = true
					}
				});
				if(!found) {
					$(this).hide();
				} else {
					$(this).show();
				}
			});
		});
	});
});